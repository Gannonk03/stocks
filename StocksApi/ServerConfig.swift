//
//  ServerConfig.swift
//  Stocks
//
//  Created by Kevin Gannon on 4/26/20.
//  Copyright © 2020 Kevin Gannon. All rights reserved.
//

import Foundation

enum EnvironmentType: String {
    case production = "production"
}

protocol ServerConfigType {
    var apiBaseURL: String { get }
    var apiToken: String { get }
    var environment: EnvironmentType { get }
}

struct ServerConfig: ServerConfigType {
    let apiBaseURL: String
    let apiToken: String
    let environment: EnvironmentType
    
    fileprivate init(
        apiBaseURL: String,
        apiToken: String,
        environment: EnvironmentType
    ) {
        self.apiToken = apiToken
        self.apiBaseURL = apiBaseURL
        self.environment = environment
    }
    
    static let production: ServerConfigType = ServerConfig(
        apiBaseURL: "https://\(Secrets.Api.Endpoints.production)",
        apiToken: Secrets.Api.Client.token,
        environment: .production
    )
    
    static func config(for environment: EnvironmentType) -> ServerConfigType {
        switch environment {
        case .production:
            return ServerConfig.production
        }
    }
}
