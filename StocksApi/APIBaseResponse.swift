import Foundation
import Argo

enum APIError: Error {
    case Network(error: Error)
    case CouldNotParse
    
    var localizedDescription: String {
        return Strings.API_Error_Generic()
    }
}

struct APIResponse<T: Argo.Decodable> {
    var data: T?
    var apiError: APIError?
    
    init(
        data: T? = nil,
        apiError: APIError? = nil
    ) {
        self.data = data
        self.apiError = apiError
    }
}
