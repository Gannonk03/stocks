//
//  Routes.swift
//  Stocks
//
//  Created by Kevin Gannon on 4/26/20.
//  Copyright © 2020 Kevin Gannon. All rights reserved.
//
import Alamofire

enum Route {
    case stockSymbols
    case quote(_ symbol: String)
    case candleSticks(_ symbol: String, _ count: String, _ resolution: String)
    
    var requestProperties: (
        method: HTTPMethod,
        path: String,
        query: [String:Any]
        ) {
        switch self {
        case .stockSymbols:
            return (.get, "/stock/symbol", ["exchange": "US"])
        case let .quote(symbol):
            return (.get, "/quote", ["symbol": symbol])
        case let .candleSticks(symbol, count, resolution):
            return (.get, "/stock/candle", [
                "symbol": symbol,
                "resolution": resolution,
                "count": count
            ])
        }
    }
}
