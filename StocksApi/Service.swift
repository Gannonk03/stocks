//
//  Service.swift
//  Stocks
//
//  Created by Kevin Gannon on 4/26/20.
//  Copyright © 2020 Kevin Gannon. All rights reserved.
//

import Prelude
import RxSwift
import RxCocoa
import Alamofire
import Argo

protocol ServiceType {
    var serverConfig: ServerConfigType { get }
    
    func fetchTickerSymbols() -> Observable<APIResponse<TickerSymbolEnvelope>>
    func fetchQuote(_ symbol: String) -> Observable<APIResponse<Quote>>
    func fetchCandleStickData(_ symbol: String)
        -> Observable<APIResponse<CandleSticksEnvelope>>
}

struct Service: ServiceType {
    let serverConfig: ServerConfigType
    
    init(serverConfig: ServerConfigType = ServerConfig.production) {
        self.serverConfig = serverConfig
    }
    
    fileprivate func request<T: Argo.Decodable>(_ route: Route)
        -> Observable<APIResponse<T>> where T == T.DecodedType {
            var props = route.requestProperties
            props.query["token"] = serverConfig.apiToken
            let path = serverConfig.apiBaseURL + props.path
            
            return Observable.create { observer in
                let req = AF.request(
                    path,
                    method: props.method,
                    parameters: props.query
                ).responseJSON { data in
                        switch data.result {
                        case .success(let json):
                            if let res: T = decode(json) {
                                observer.onNext(APIResponse<T>(data: res))
                                return
                            }
                            observer.onNext(APIResponse<T>(
                                apiError: .CouldNotParse
                            ))
                        case .failure(let error):
                            observer.onNext(APIResponse<T>(
                                apiError: .Network(error: error)
                            ))
                    }
                }
                
            return Disposables.create { req.cancel() }
        }
    }
    
    func fetchCandleStickData(_ symbol: String)
        -> Observable<APIResponse<CandleSticksEnvelope>> {
        request(.candleSticks(symbol, "365", "D"))
    }
    
    func fetchQuote(_ symbol: String) -> Observable<APIResponse<Quote>> {
        request(.quote(symbol))
    }
    
    func fetchTickerSymbols() -> Observable<APIResponse<TickerSymbolEnvelope>> {
        request(.stockSymbols)
    }
}
