//
//  TickerSymbol.swift
//  Stocks
//
//  Created by Kevin Gannon on 4/26/20.
//  Copyright © 2020 Kevin Gannon. All rights reserved.
//

import Argo
import Runes
import Curry

struct TickerSymbol {
    let description: String
    let displaySymbol: String
    let symbol: String
}

extension TickerSymbol: Argo.Decodable {
    static func decode(_ json: JSON) -> Decoded<TickerSymbol> {
        curry(TickerSymbol.init)
            <^> json <| "description"
            <*> json <| "displaySymbol"
            <*> json <| "symbol"
    }
}
