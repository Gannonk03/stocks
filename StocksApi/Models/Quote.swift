//
//  Quote.swift
//  Stocks
//
//  Created by Kevin Gannon on 4/27/20.
//  Copyright © 2020 Kevin Gannon. All rights reserved.
//

import Argo
import Runes
import Curry

struct Quote {
    let open: Double
    let high: Double
    let low: Double
    let current: Double
    let previousClose: Double
}

extension Quote: Argo.Decodable {
    static func decode(_ json: JSON) -> Decoded<Quote> {
        curry(Quote.init)
            <^> json <| "o"
            <*> json <| "h"
            <*> json <| "l"
            <*> json <| "c"
            <*> json <| "pc"
    }
}
