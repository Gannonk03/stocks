//
//  TickerSymbolEnvelope.swift
//  Stocks
//
//  Created by Kevin Gannon on 4/26/20.
//  Copyright © 2020 Kevin Gannon. All rights reserved.
//

import Argo
import Runes
import Curry

struct TickerSymbolEnvelope {
    let tickerSymbols: [TickerSymbol]
}

extension TickerSymbolEnvelope: Argo.Decodable {
    static func decode(_ json: JSON) -> Decoded<TickerSymbolEnvelope> {
        curry(TickerSymbolEnvelope.init) <^> [TickerSymbol].decode(json)
    }
}

