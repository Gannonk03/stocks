//
//  CandleSticksEnvelope.swift
//  Stocks
//
//  Created by Kevin Gannon on 4/27/20.
//  Copyright © 2020 Kevin Gannon. All rights reserved.
//

import Foundation
import Argo
import Runes
import Curry

struct CandleSticksEnvelope {
    let openPrices: [Double]
    let highPrices: [Double]
    let lowPrices: [Double]
    let closePrices: [Double]
    let timestamps: [Double]
    let status: String
}

extension CandleSticksEnvelope: Argo.Decodable {
    static func decode(_ json: JSON) -> Decoded<CandleSticksEnvelope> {
        curry(CandleSticksEnvelope.init)
            <^> json <|| "o"
            <*> json <|| "h"
            <*> json <|| "l"
            <*> json <|| "c"
            <*> json <|| "t"
            <*> json <| "s"
    }
}
