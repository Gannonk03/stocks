//
//  Double.swift
//  Stocks
//
//  Created by Kevin Gannon on 4/28/20.
//  Copyright © 2020 Kevin Gannon. All rights reserved.
//

import Foundation

extension Double {
    var rounded: String {
        let multiplier = pow(10.0, Double(2))
        let rounded = (self * multiplier).rounded() / multiplier
        if floor(rounded) == rounded { return "\(Int(rounded))" }
        return "\(rounded)"
    }
}
