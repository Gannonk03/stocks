//
//  Colors.swift
//  Stocks
//
//  Created by Kevin Gannon on 4/25/20.
//  Copyright © 2020 Kevin Gannon. All rights reserved.
//

import UIKit

extension UIColor {
    public static var ss_background: UIColor {
      return .hex(0x2e2e38)
    }
    
    public static var ss_surface: UIColor {
      return .hex(0x373740)
    }
    
    public static var ss_error: UIColor {
        return .hex(0xeb6457)
    }
    
    public static var ss_orange: UIColor {
        return .hex(0xFF6859)
    }
    
    public static var ss_yellow: UIColor {
        return .hex(0xFFCF44)
    }
    
    public static var ss_green: UIColor {
        return .hex(0x1EB980)
    }
    
    public static var ss_darkGreen: UIColor {
        return .hex(0x045D56)
    }
}
