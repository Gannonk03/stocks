//
//  Keyboard.swift
//  Stocks
//
//  Created by Kevin Gannon on 4/25/20.
//  Copyright © 2020 Kevin Gannon. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

final class Keyboard {
    typealias Change = (
        frame: CGRect,
        duration: TimeInterval,
        options: UIView.AnimationOptions,
        notificationName: Notification.Name
    )
    
    static let shared = Keyboard()
    
    private let _change = PublishSubject<Change>()
    static var change: Observable<Change> {
        return self.shared._change.asObservable()
    }
        
    private init() {
        NotificationCenter.default.addObserver(
          self,
          selector: #selector(self.change(_:)),
          name: UIResponder.keyboardWillShowNotification,
          object: nil
        )

        NotificationCenter.default.addObserver(
          self,
          selector: #selector(self.change(_:)),
          name: UIResponder.keyboardWillHideNotification,
          object: nil
        )
    }
    
    @objc private func change(_ notification: Notification) {
        guard let userInfo = notification.userInfo,
          let frame = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue,
          let duration = userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber,
          let curveNumber = userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber,
          let curve = UIView.AnimationCurve(rawValue: curveNumber.intValue)
        else {
          return
        }
        
        self._change.onNext((
            frame.cgRectValue,
            duration.doubleValue,
            UIView.AnimationOptions(rawValue: UInt(curve.rawValue)),
            notificationName: notification.name
        ))
    }
}
