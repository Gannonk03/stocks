//
//  Environment.swift
//  Stocks
//
//  Created by Kevin Gannon on 4/26/20.
//  Copyright © 2020 Kevin Gannon. All rights reserved.
//

import Foundation

struct Environment {
    let apiService: ServiceType
    
    var environmentType: EnvironmentType {
        return self.apiService.serverConfig.environment
    }
    
    init(apiService: ServiceType = Service()) {
        self.apiService = apiService
    }
}
