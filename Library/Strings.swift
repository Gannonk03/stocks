//
//  Strings.swift
//  Stocks
//
//  Created by Kevin Gannon on 4/27/20.
//  Copyright © 2020 Kevin Gannon. All rights reserved.
//

enum Strings {
    static func Ticker_Symbol_Error() -> String {
        "Invalid ticker symbol"
    }
    
    static func Ticker_Symbol_Textfield_Label() -> String {
        "Ticker Symbol"
    }
    
    static func API_Error_Generic() -> String {
        "Oops, something went wrong..."
    }
    
    static func GO_Button_Text() -> String {
        "GO"
    }
}

enum HeroIDS {
    static func Splash_Icon() -> String {
        "Icon"
    }
}
