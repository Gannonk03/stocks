//
//  StockDetailStyles.swift
//  Stocks
//
//  Created by Kevin Gannon on 4/28/20.
//  Copyright © 2020 Kevin Gannon. All rights reserved.
//

import Prelude
import Prelude_UIKit
import UIKit

let detailHeaderStyle =
    UILabel.lens.textColor .~ .white <>
    UILabel.lens.font .~ UIFont(name: "AvenirNext-DemiBold", size: 17)!

let detailHeaderBigStyle =
    UILabel.lens.textColor .~ .white <>
    UILabel.lens.font .~ UIFont(name: "AvenirNext-DemiBold", size: 26)!

let detailWidgetStyle =
    UIView.lens.backgroundColor .~ .ss_surface <>
    UIView.lens.layer.cornerRadius .~ 6.0 <>
    UIView.lens.layer.masksToBounds .~ false <>
    UIView.lens.layer.shadowColor .~ UIColor.black.cgColor <>
    UIView.lens.layer.shadowOffset .~ .zero <>
    UIView.lens.layer.shadowRadius .~ 5 <>
    UIView.lens.layer.shadowOpacity .~ Float(0.2)
