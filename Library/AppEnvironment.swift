//
//  AppEnvironment.swift
//  Stocks
//
//  Created by Kevin Gannon on 4/26/20.
//  Copyright © 2020 Kevin Gannon. All rights reserved.
//

import Foundation
import Prelude
import RxCocoa
import RxSwift
import Runes
import Argo

struct AppEnvironment {
    static private(set) var current: Environment = Environment()
    
    static func setEnvironment(_ environment: Environment) {
        AppEnvironment.current = environment
    }
}
