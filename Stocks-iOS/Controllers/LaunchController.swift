//
//  LaunchController.swift
//  Stocks
//
//  Created by Kevin Gannon on 4/30/20.
//  Copyright © 2020 Kevin Gannon. All rights reserved.
//

import UIKit
import Hero

class LaunchController: UIViewController {
    @IBOutlet var icon: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()

        hero.isEnabled = true

        icon.hero.id = HeroIDS.Splash_Icon()

        navigationController?.hero.navigationAnimationType = .fade
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {[weak self] in
            self?.performSegue(withIdentifier: "selectTickerVC", sender: nil)
        }
    }
}
