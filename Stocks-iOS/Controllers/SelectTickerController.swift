//
//  SelectTickerController.swift
//  Stocks
//
//  Created by Kevin Gannon on 4/24/20.
//  Copyright © 2020 Kevin Gannon. All rights reserved.
//

import UIKit
import Prelude
import Prelude_UIKit
import RxCocoa
import RxSwift

final class SelectTickerController: UIViewController {
    @IBOutlet fileprivate weak var tickerInputLabel: UITextField!
    @IBOutlet fileprivate weak var goButton: UIButton!
    @IBOutlet fileprivate weak var goButtonBottomConstraint: NSLayoutConstraint!
    @IBOutlet fileprivate weak var errorLabel: UILabel!
    @IBOutlet fileprivate weak var tickerInputY: NSLayoutConstraint!
    @IBOutlet fileprivate weak var tickerInputBorder: InputLabelBorderView!
    @IBOutlet fileprivate weak var errorLabelTopSpace: NSLayoutConstraint!
    @IBOutlet fileprivate weak var icon: UIImageView!
    
    fileprivate let viewModel: SelectTickerViewModelType = SelectTickerViewModel()
    fileprivate let disposeBag = DisposeBag()
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.icon.hero.id = HeroIDS.Splash_Icon()
        self.bindStyles()
        self.bindViewModel()
        self.tickerInputLabel.delegate = self
        self.tickerInputLabel.addTarget(
            self,
            action: #selector(self.tickerChanged(_:)),
            for: [.editingChanged]
        )
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.viewModel.inputs.viewWillAppear()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        DispatchQueue.main.async {[weak self] in
            self?.tickerInputLabel.becomeFirstResponder()
        }
    }
    
    @objc func tickerChanged(_ textField: UITextField) {
        self.viewModel.inputs.tickerChanged(textField.text ?? "" )
    }
    
    @IBAction func goButtonPressed(_ sender: Any) {
        self.tickerInputLabel.resignFirstResponder()
        self.viewModel.inputs.goButtonPressed()
    }
    
    @IBAction func unwind(_ seg: UIStoryboardSegue) {}
    
    private func bindStyles() {
        _ = self.goButton
            |> UIButton.lens.titleLabel.font .~ UIFont.systemFont(ofSize: 17)
            |> UIButton.lens.titleColor(for: .normal) .~ UIColor.white
            |> UIButton.lens.titleColor(for: .disabled) .~ UIColor.gray
            |> UIButton.lens.backgroundColor .~ .ss_surface
            |> UIButton.lens.title(for: .normal) .~ Strings.GO_Button_Text()
        
        _ = self.errorLabel
            |> UILabel.lens.textColor .~ .ss_error
            |> UILabel.lens.font .~ UIFont.systemFont(ofSize: 15.0)
        
        _ = self.tickerInputLabel
            |> UITextField.lens.textColor .~ .white
            |> UITextField.lens.tintColor .~ .white
        
        _ = self.view |> UIView.lens.backgroundColor .~ .ss_background
    }
    
    private func bindViewModel() {
        self.viewModel.outputs.isGoButtonEnabled
            .bind(to: goButton.rx.isEnabled)
            .disposed(by: disposeBag)
        
        self.viewModel.outputs.isTickerInputFieldEnabled
            .bind(to: tickerInputLabel.rx.isEnabled)
            .disposed(by: disposeBag)
        
        self.viewModel.outputs.selectTickerInputColor
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] next in
                self?.tickerInputBorder.bottom.layer.borderColor = next.cgColor
                self?.tickerInputBorder.topLeft.layer.borderColor = next.cgColor
                self?.tickerInputBorder.topRight.layer.borderColor = next.cgColor
                self?.tickerInputBorder.label.textColor = next
        }).disposed(by: disposeBag)
        
        self.viewModel.outputs.stockDetailViewModel
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: {[weak self] next in
                guard let vc = self?.storyboard?.instantiateViewController(
                    identifier: "StockDetailVC",
                    creator: { coder in
                    return StockDetailViewController(coder: coder, viewModel: next)
                }) else { fatalError("Failed to load StockDetailController.") }
                self?.navigationController?.pushViewController(vc, animated: true)
        }).disposed(by: disposeBag)
        
        self.viewModel.outputs.showError
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [ weak self] next in
                self?.errorLabel.alpha = next == nil ? 0.0 : 1.0
                self?.errorLabel.text = next
        }).disposed(by: disposeBag)
        
        Keyboard.change
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] change in
                let inset = self?.view.safeAreaInsets.bottom ?? 0
                self?.goButtonBottomConstraint.constant =
                    change.notificationName == UIResponder.keyboardWillHideNotification
                    ? 0
                    : change.frame.height - inset + 8
                self?.tickerInputY.constant =
                    change.notificationName == UIResponder.keyboardWillHideNotification ? 0 : -50
                UIView.animate(
                    withDuration: change.duration,
                    delay: 0.0,
                    options: change.options,
                    animations: {
                    self?.view.layoutIfNeeded()
                }, completion: nil)
        }).disposed(by: disposeBag)
    }
}

extension SelectTickerController: UITextFieldDelegate {
    func textField(
        _ textField: UITextField,
        shouldChangeCharactersIn range: NSRange,
        replacementString string: String
    ) -> Bool {
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return
            newString.length <= 5 &&
            newString.rangeOfCharacter(
                from: CharacterSet.uppercaseLetters.inverted).location == NSNotFound
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let text = textField.text, !text.isEmpty {
            self.viewModel.inputs.goButtonPressed()
            textField.resignFirstResponder()
        }
        return false
    }
}

