//
//  StockDetailViewController.swift
//  Stocks
//
//  Created by Kevin Gannon on 4/27/20.
//  Copyright © 2020 Kevin Gannon. All rights reserved.
//

import UIKit
import Prelude
import Prelude_UIKit
import RxCocoa
import RxSwift
import Charts
import Hero

class StockDetailViewController: UIViewController {
    fileprivate let viewModel: StockDetailViewModelType
    
    @IBOutlet fileprivate weak var chartView: LineChartView!
    @IBOutlet fileprivate weak var chartHeaderLabel: UILabel!
    @IBOutlet fileprivate weak var lowPriceHeader: UILabel!
    @IBOutlet fileprivate weak var highPriceHeader: UILabel!
    @IBOutlet fileprivate weak var openPriceHeader: UILabel!
    @IBOutlet fileprivate weak var stockLowPrice: UILabel!
    @IBOutlet fileprivate weak var stockHighPrice: UILabel!
    @IBOutlet fileprivate weak var stockOpenPrice: UILabel!
    @IBOutlet fileprivate weak var stockSymbolLabel: UILabel!
    @IBOutlet fileprivate weak var stockCurrentPrice: UILabel!
    @IBOutlet fileprivate weak var stockDescription: UILabel!
    @IBOutlet fileprivate weak var tickerWidget: UIView!
    @IBOutlet fileprivate weak var scrollView: UIScrollView!
    @IBOutlet fileprivate weak var dailyWidget: UIView!
    @IBOutlet fileprivate weak var chartWidget: UIView!
    
    fileprivate let disposeBag = DisposeBag()
    
    init?(coder: NSCoder, viewModel: StockDetailViewModelType) {
        self.viewModel = viewModel
        super.init(coder: coder)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bindStyles()
        bindViewModel()
        tickerWidget.hero.modifiers = [
            .fade,
            .translate(y:50),
            .duration(0.3),
            .delay(0.1)
        ]
        dailyWidget.hero.modifiers = [
            .fade,
            .translate(y:50),
            .duration(0.3),
            .delay(0.05)
        ]
        chartWidget.hero.modifiers = [
            .fade,
            .translate(y:50),
            .duration(0.3),
            .delay(0)
        ]
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        tickerWidget.hero.modifiers = [.fade, .translate(y:50), .duration(0.3)]
        dailyWidget.hero.modifiers = [.fade, .translate(y:50), .duration(0.3)]
        chartWidget.hero.modifiers = [.fade, .translate(y:50), .duration(0.3)]
    }
    
    private func bindViewModel() {
        self.viewModel.outputs.stockSymbol
            .bind(to: stockSymbolLabel.rx.text)
            .disposed(by: disposeBag)
        
        self.viewModel.outputs.stockDescription
            .bind(to: stockDescription.rx.text)
            .disposed(by: disposeBag)
        
        self.viewModel.outputs.currentPrice
            .bind(to: stockCurrentPrice.rx.text)
            .disposed(by: disposeBag)
        
        self.viewModel.outputs.openPrice
            .bind(to: stockOpenPrice.rx.text)
            .disposed(by: disposeBag)
        
        self.viewModel.outputs.highPrice
            .bind(to: stockHighPrice.rx.text)
            .disposed(by: disposeBag)
        
        self.viewModel.outputs.lowPrice
            .bind(to: stockLowPrice.rx.text)
            .disposed(by: disposeBag)
        
        self.viewModel.outputs.candles
            .asObservable()
            .subscribe(onNext: {[weak self] next in
                let entries = next.enumerated().map({ next -> ChartDataEntry in
                    let (offset, balance) = next
                    return ChartDataEntry(x: Double(offset), y: balance)
                })
                let set = LineChartDataSet(entries: entries, label: nil)
                set.lineDashLengths = [5, 2.5]
                set.highlightLineDashLengths = [5, 2.5]
                set.setColor(.clear)
                set.setCircleColor(.clear)
                set.lineWidth = 0
                set.circleRadius = 0
                set.drawCircleHoleEnabled = false
                set.drawValuesEnabled = false

                let gradientColors = [
                    UIColor.ss_darkGreen.cgColor,
                    UIColor.ss_green.cgColor
                ]
                
                let gradient = CGGradient(
                    colorsSpace: nil,
                    colors: gradientColors as CFArray, locations: nil
                )!

                set.fillAlpha = 1
                set.fill = Fill(linearGradient: gradient, angle: 90)
                set.drawFilledEnabled = true

                let data = LineChartData(dataSet: set)

                self?.chartView.data = data
        }).disposed(by: disposeBag)
    }
    
    private func bindStyles() {
        _ = self.view |> UIView.lens.backgroundColor .~ .ss_background
        _ = self.tickerWidget |> detailWidgetStyle
        _ = self.dailyWidget |> detailWidgetStyle
        _ = self.chartWidget |> detailWidgetStyle
        _ = self.stockDescription |> detailHeaderBigStyle
        _ = self.stockCurrentPrice |> detailHeaderBigStyle
        _ = self.stockSymbolLabel |> detailHeaderStyle
        _ = self.stockOpenPrice |> detailHeaderStyle
        _ = self.stockHighPrice |> detailHeaderStyle
        _ = self.stockLowPrice |> detailHeaderStyle
        _ = self.lowPriceHeader |> detailHeaderStyle
        _ = self.highPriceHeader |> detailHeaderStyle
        _ = self.openPriceHeader |> detailHeaderStyle
        _ = self.chartHeaderLabel |> detailHeaderStyle
    }
}
