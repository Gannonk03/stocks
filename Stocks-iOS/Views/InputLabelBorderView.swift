import UIKit
import Prelude
import Prelude_UIKit

class InputLabelBorderView: UIView {
    let bottom = UIView(frame: .zero)
    let topLeft = UIView(frame: .zero)
    let topRight = UIView(frame: .zero)
    let label = UILabel(frame: .zero)

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        backgroundColor = .clear
        configureViews()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = .clear
        configureViews()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        updateMaskConstraints()
    }
    
    private func updateMaskConstraints() {
        let bottomMask = UIView(frame: CGRect(
            x: 0.0,
            y: frame.height / 2.0,
            width: frame.width,
            height: frame.height / 2.0
        ))
        
        _ = bottomMask |> UIView.lens.backgroundColor .~ .black
        
        bottom.mask = bottomMask
        
        let topLeftMask = UIView(frame: CGRect(
            x: 0.0,
            y: 0.0,
            width: 15.0,
            height: frame.height / 2.0
        ))
               
        _ = topLeftMask |> UIView.lens.backgroundColor .~ .black
               
        topLeft.mask = topLeftMask
        
        let topRightMask = UIView(frame: CGRect(
            x: 22.0 + label.frame.width + 8.0,
            y: 0.0,
            width: frame.width,
            height: frame.height / 2.0
        ))
               
        _ = topRightMask |> UIView.lens.backgroundColor .~ .black
               
        topRight.mask = topRightMask
    }
    
    private func configureViews() {
        addSubview(bottom)
        
        bottom.translatesAutoresizingMaskIntoConstraints = false
        bottom.heightAnchor.constraint(equalTo: heightAnchor).isActive = true
        bottom.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        bottom.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        
        _ = bottom
            |> UIView.lens.layer.borderColor .~ UIColor.white.cgColor
            |> UIView.lens.layer.borderWidth .~ 1.0
            |> UIView.lens.layer.cornerRadius .~ 6.0
                                        
        addSubview(label)
        
        _ = label
            |> UILabel.lens.font .~ UIFont.systemFont(ofSize: 16.0)
            |> UILabel.lens.text .~ Strings.Ticker_Symbol_Textfield_Label()
            |> UILabel.lens.textColor .~ .white

        label.sizeToFit()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.centerYAnchor.constraint(equalTo: topAnchor, constant: 0).isActive = true
        label.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 22.0).isActive = true
        
        addSubview(topLeft)
        
        topLeft.translatesAutoresizingMaskIntoConstraints = false
        topLeft.heightAnchor.constraint(equalTo: heightAnchor).isActive = true
        topLeft.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        topLeft.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        
        _ = topLeft
            |> UIView.lens.layer.borderColor .~ UIColor.white.cgColor
            |> UIView.lens.layer.borderWidth .~ 1.0
            |> UIView.lens.layer.cornerRadius .~ 6.0
        
        addSubview(topRight)
        
        topRight.translatesAutoresizingMaskIntoConstraints = false
        topRight.heightAnchor.constraint(equalTo: heightAnchor).isActive = true
        topRight.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        topRight.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        
        _ = topRight
            |> UIView.lens.layer.borderColor .~ UIColor.white.cgColor
            |> UIView.lens.layer.borderWidth .~ 1.0
            |> UIView.lens.layer.cornerRadius .~ 6.0
    }
}
