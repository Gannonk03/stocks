//
//  StockDetailViewModel.swift
//  Stocks
//
//  Created by Kevin Gannon on 4/27/20.
//  Copyright © 2020 Kevin Gannon. All rights reserved.
//

import RxSwift
import RxCocoa
import UIKit

protocol StockDetailViewModelOutputs {
    var stockSymbol: Observable<String> { get }
    var stockDescription: Observable<String> { get }
    var currentPrice: Observable<String> { get }
    var openPrice: Observable<String> { get }
    var highPrice: Observable<String> { get }
    var lowPrice: Observable<String> { get }
    var candles: Observable<[Double]> { get }
}

protocol StockDetailViewModelType {
    var outputs: StockDetailViewModelOutputs { get }
}

struct StockDetailViewModel: StockDetailViewModelType, StockDetailViewModelOutputs {
    fileprivate let quote: Quote
    fileprivate let ticker: TickerSymbol
    fileprivate let candlesEnvelope: CandleSticksEnvelope
    
    init(quote: Quote, ticker: TickerSymbol, candles: CandleSticksEnvelope) {
        self.quote = quote
        self.ticker = ticker
        self.candlesEnvelope = candles
        self._stockSymbol = BehaviorRelay<String>(value: ticker.displaySymbol)
        self.stockSymbol = self._stockSymbol.asObservable()
        self._stockDescription = BehaviorRelay<String>(value: ticker.description)
        self.stockDescription = self._stockDescription.asObservable()
        self._current = BehaviorRelay<String>(value: "$\(quote.current.rounded)")
        self.currentPrice = self._current.asObservable()
        self._open = BehaviorRelay<String>(value: "\(quote.open.rounded)")
        self.openPrice = self._open.asObservable()
        self._high = BehaviorRelay<String>(value: "\(quote.high.rounded)")
        self.highPrice = self._high.asObservable()
        self._low = BehaviorRelay<String>(value: "\(quote.low.rounded)")
        self.lowPrice = self._low.asObservable()
        self._candles = BehaviorRelay<[Double]>(value: candles.closePrices)
        self.candles = self._candles.asObservable()
    }
    
    fileprivate var _stockSymbol: BehaviorRelay<String>
    var stockSymbol: Observable<String>
    
    fileprivate var _stockDescription: BehaviorRelay<String>
    var stockDescription: Observable<String>
    
    fileprivate var _current: BehaviorRelay<String>
    var currentPrice: Observable<String>
    
    fileprivate var _open: BehaviorRelay<String>
    var openPrice: Observable<String>
    
    fileprivate var _high: BehaviorRelay<String>
    var highPrice: Observable<String>
    
    fileprivate var _low: BehaviorRelay<String>
    var lowPrice: Observable<String>
    
    fileprivate var _candles: BehaviorRelay<[Double]>
    var candles: Observable<[Double]>
    
    var outputs: StockDetailViewModelOutputs { return self }
}
