//
//  SelectTickerViewModel.swift
//  Stocks
//
//  Created by Kevin Gannon on 4/25/20.
//  Copyright © 2020 Kevin Gannon. All rights reserved.
//

import RxSwift
import RxCocoa
import UIKit

protocol SelectTickerViewModelInputs {
    /// Call when the view will appear.
    func viewWillAppear()
    
    /// Call when the user updates the ticker.
    func tickerChanged(_ ticker: String)
    
    /// Call when go button is pressed.
    func goButtonPressed()
}

protocol SelectTickerViewModelOutputs {
    /// Whether or not the go button should be active & tappable
    var isGoButtonEnabled: Observable<Bool> { get }
    
    /// Whether or not the input field should be active & tappable
     var isTickerInputFieldEnabled: Observable<Bool> { get }
    
    /// Emits when a ticker request error has occurred and a message should be displayed.
    var showError: Observable<String?> { get }
    
    /// Emits the next border & text color for select ticker input field
    var selectTickerInputColor: Observable<UIColor> { get }
    
    /// Emits the stockDetailViewModel when ticker data is validated
    var stockDetailViewModel: Observable<StockDetailViewModel> { get }
}

protocol SelectTickerViewModelType {
    var inputs: SelectTickerViewModelInputs { get }
    var outputs: SelectTickerViewModelOutputs { get }
}

final class SelectTickerViewModel: SelectTickerViewModelType,
    SelectTickerViewModelInputs,
    SelectTickerViewModelOutputs {
    
    init() {
        self.isGoButtonEnabled = _isGoButtonEnabled.asObservable()
        self.showError = _showError.asObservable()
        self.selectTickerInputColor = _selectTickerInputColor.asObservable()
        self.stockDetailViewModel = _stockDetailViewModel.asObservable()
        self.isTickerInputFieldEnabled = _isTickerInputFieldEnabled.asObservable()
        
        let goBtnPressedObservable = _goButtonPressed.asObservable().share()
        let viewWillAppearObservable = _viewWillAppear.asObservable().share()
        
        let tickerSymbolsObservale = goBtnPressedObservable.flatMap { _ in
            AppEnvironment.current.apiService.fetchTickerSymbols()
        }.share()
        
        viewWillAppearObservable
            .withLatestFrom(tickerRelay.asObservable())
            .map { !$0.isEmpty }
            .bind(to: _isGoButtonEnabled)
            .disposed(by: disposeBag)
        
        viewWillAppearObservable
            .withLatestFrom(tickerRelay.asObservable())
            .map { !$0.isEmpty }
            .skip(1)
            .bind(to: _isTickerInputFieldEnabled)
            .disposed(by: disposeBag)
        
        tickerRelay.asObservable()
            .map { !$0.isEmpty }
            .bind(to: _isGoButtonEnabled)
            .disposed(by: disposeBag)
        
        goBtnPressedObservable.subscribe(onNext: {[weak self] next in
            self?._showError.accept(nil)
            self?._selectTickerInputColor.accept(.white)
            self?._isGoButtonEnabled.accept(false)
            self?._isTickerInputFieldEnabled.accept(false)
        }).disposed(by: disposeBag)
        
        showError.compactMap{ $0 }.subscribe(onNext: {[weak self] _ in
            self?._isGoButtonEnabled.accept(true)
            self?._selectTickerInputColor.accept(.ss_error)
            self?._isTickerInputFieldEnabled.accept(true)
        }).disposed(by: disposeBag)
        
        let validTickerSymbol = tickerSymbolsObservale
            .compactMap {[weak self] next -> TickerSymbol? in
            guard let ss = self, let data = next.data else {
                self?._showError.accept(next.apiError?.localizedDescription)
                return nil
            }
            
            var dict = [String:TickerSymbol]()
            data.tickerSymbols.forEach { dict[$0.displaySymbol] = $0 }
            
            if dict[ss.tickerRelay.value] == nil {
                ss._showError.accept(Strings.Ticker_Symbol_Error())
            }
            
            return dict[ss.tickerRelay.value]
        }.share()
                
        let quoteObservable = validTickerSymbol.flatMap { next in
            AppEnvironment.current.apiService.fetchQuote(next.symbol)
        }
        
        let candlesObservable = validTickerSymbol.flatMap {
            next -> Observable<APIResponse<CandleSticksEnvelope>> in
            AppEnvironment.current.apiService.fetchCandleStickData(next.symbol)
        }
        
        let stockDataObservable = Observable.zip(
            quoteObservable,
            candlesObservable
        ).share()
        
        stockDataObservable.compactMap { s -> String? in
            let (quote,candles) = (s.0,s.1)
            
            if let quoteError = quote.apiError {
                return quoteError.localizedDescription
            }
            
            if let candlesError = candles.apiError {
                return candlesError.localizedDescription
            }
            
            if let data = candles.data, data.status != "ok" {
                return Strings.API_Error_Generic()
            }
    
            return nil
        }
        .bind(to: _showError)
        .disposed(by: disposeBag)
        
        stockDataObservable.withLatestFrom(validTickerSymbol) {
            stockData, ticker -> StockDetailViewModel? in
            let (quote,candles) = (stockData.0,stockData.1)
            
            guard
                let q = quote.data,
                let c = candles.data,
                c.status == "ok" else { return nil }
        
            return StockDetailViewModel(quote: q, ticker: ticker, candles: c)
        }
        .compactMap { $0 }
        .bind(to: _stockDetailViewModel)
        .disposed(by: disposeBag)
    }
    
    fileprivate let _viewWillAppear = PublishSubject<Void>()
    func viewWillAppear() {
        _viewWillAppear.onNext(())
    }
    
    fileprivate let tickerRelay = BehaviorRelay<String>(value: "")
    func tickerChanged(_ ticker: String) {
        tickerRelay.accept(ticker)
    }
    
    fileprivate let _goButtonPressed = PublishSubject<Void>()
    func goButtonPressed() {
        _goButtonPressed.onNext(())
    }
        
    fileprivate var _showError = BehaviorRelay<String?>(value: nil)
    var showError: Observable<String?>
    
    fileprivate var _selectTickerInputColor = BehaviorRelay<UIColor>(value: .white)
    var selectTickerInputColor: Observable<UIColor>
        
    fileprivate var _isGoButtonEnabled = BehaviorRelay<Bool>(value: false)
    var isGoButtonEnabled: Observable<Bool>
    
    fileprivate var _stockDetailViewModel = PublishSubject<StockDetailViewModel>()
    var stockDetailViewModel: Observable<StockDetailViewModel>
        
    fileprivate var _isTickerInputFieldEnabled = BehaviorRelay<Bool>(value: true)
    var isTickerInputFieldEnabled: Observable<Bool>
    
    fileprivate let disposeBag = DisposeBag()
    
    var inputs: SelectTickerViewModelInputs { return self }
    var outputs: SelectTickerViewModelOutputs { return self }
}
