//
//  StocksTests.swift
//  StocksTests
//
//  Created by Kevin Gannon on 4/24/20.
//  Copyright © 2020 Kevin Gannon. All rights reserved.
//

import XCTest
import RxTest
import RxSwift
import RxCocoa
@testable import Stocks

class StocksTests: XCTestCase {
    var scheduler: TestScheduler!
    var disposeBag: DisposeBag!

    override func setUpWithError() throws {
        scheduler = TestScheduler(initialClock: 0)
        disposeBag = DisposeBag()
    }

    func testIsGoBtnEnabled() {
        let segueObserver = scheduler.createObserver(Bool.self)
        let sut: SelectTickerViewModelType = SelectTickerViewModel()
        
        sut.outputs.isGoButtonEnabled.bind(to: segueObserver).disposed(by: disposeBag)
        
        scheduler.createColdObservable([
            .next(5, ("not emtpy")),
            .next(10, (""))
        ])
        .subscribe(onNext: { sut.inputs.tickerChanged($0) })
        .disposed(by: disposeBag)
        
        scheduler.start()

        XCTAssertEqual(segueObserver.events, [
            .next(0,false),
            .next(5, true),
            .next(10, false)
        ])
    }
}
